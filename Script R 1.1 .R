#########################################################
#SCRIPT R : stage Quentin Hirtzlin - 26/04/21 - 05/10/21#
#########################################################
#1.1 

#### PACKAGES/LECTURES DATAS/FUSIONS/VERIFICATIONS DOUBLONS/TRANSFORMATION OBJET IGRAPH ####

#packages utilisés :

library(dplyr)
library(igraph)
library(readr)
library(ggplot2)
library(questionr)

#Lecture des données par R

social_graph_RECORDS_wave2_out <- read_csv("Social graph/social_graph_RECORDS_wave2_filtered.csv")
social_graph_RECORDS_wave2_in <- read_csv("Social graph/social_graph_RECORDS_wave2_in.csv")
social_graph_RECORDS_wave2_neighborhood_links <- read_csv("Social graph/social_graph_RECORDS_wave2_neighborhood_links.csv")



#Première fusion 
Data<-full_join(social_graph_RECORDS_wave2_out,social_graph_RECORDS_wave2_in)

#Fusion finale 
Dataf<-full_join(Data,social_graph_RECORDS_wave2_neighborhood_links)


#Y a t-il des doublons ? 

duplicates<-duplicated(Dataf)

table(duplicates) #Que du FALSE => Il n'y a pas de doublons

#transformation en objet igraph

Graph<- graph_from_data_frame(Dataf, directed = TRUE)


#### Premières approches quantis ####

#Indicateurs sur les données entrantes et sortantes

DegIn<-degree(Graph,mode = "in")
DegOut<-degree(Graph,mode = "out")

summary(DegIn)
summary(DegOut)



#Permet d'avoir les identifiants des comptes qui sont beaucoup suivis et qui suivent beaucoup

highfollowers<-sort(DegIn,decreasing = TRUE)
head(highfollowers)

highfollowing<-sort(DegOut,decreasing = TRUE)
head(highfollowing)
#Gros problème avec le (1), erreur dans les données ? 

#Distribution des liens 0 

DistIn<-quantile(DegIn,probs=seq(.0005,.9,by=.0005)) 
DistIn
#2.55% des users de l'échantillon sont suivis par personne

DistOut<-quantile(DegOut,probs=seq(.0005,.9,by=.0005)) #1.35 des users de l'échantillon ne suivent personnes
DistOut

#### Graphique et histogramme ####

PlotDegOutIn<-plot(DegIn,DegOut) #Distribution partiellement linéaire

HistIn<-hist(DegIn)
HistOut<-hist(DegOut)
hist(DegIn,breaks = seq(0,1300,by=1),include.lowest = TRUE,right = TRUE,)

?hist

#### Corrélation / densité du graphe / types de liens ####
#Corrélation ?
cor(DegIn,DegOut)

#Le coefficient de corrélation est d'en l'intervalle de confiance, avec une p value très faible, il y a donc un lien significatif
cor.test(DegIn,DegOut)



#Densité du graphe 
graph.density(Graph)

#Les type de liens :
dyad_census(Graph)


#L'asymétrie augmente avec


#### BOUCLE des egonetwork####

#ego network
egoN <- make_ego_graph(Graph, order = 1)

#création d'un tableau vid
d <- NULL

#boucle permettant d'analyser les ego-networks un à un et permet de  stocker les données dans le tableau
i <- 1
for (i in 1:length(egoN)){            
  ID <- V(Graph)$name[[i]]               
  nbsommets <- vcount(egoN[[i]]) 
  nbliens <- ecount(egoN[[i]])               
  densite <- graph.density(egoN[[i]])         
  transitivite <- transitivity(egoN[[i]],type = "global")
  dcens <- dyad_census(egoN[[i]])
  mutuals<-dcens$mut
  asymetrics<-dcens$asym
  propasym <- dcens$asym/(dcens$asym+dcens$mut)*100
  d <- rbind(d, data.frame(ID, nbsommets, 
                           nbliens, densite, transitivite,mutuals,asymetrics,propasym)) 
  print(i)
}

summary(d)




#### Retour à des stats ####
quantile(d$nbliens,probs=seq(.1,.9,by=.1))


quantile(d$nbsommets,probs=seq(.1,.9,by=.1))

quantile(d$propasym,probs=seq(.1,.9,by=.1))

A<-d[d$nbsommets>=42,]

summary(A)
quantile(A$propasym,probs=seq(.1,.9,by=.1))
#Il y a effectivement une distribution différentes lorsque l'on prend les égonetworks avec plus de 42 sommets (10% des Egonetworks avec le plus de sommets)
#On voit une tendance à avoir proportionnellement plus de liens asymétriques pour égonetworks avec beaucoup de sommets

selnodes<-V(egoN)[nbsommet%in%A]


